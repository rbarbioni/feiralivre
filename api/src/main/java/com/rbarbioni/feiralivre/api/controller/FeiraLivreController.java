package com.rbarbioni.feiralivre.api.controller;

import com.rbarbioni.feiralivre.core.model.FeiraLivre;
import com.rbarbioni.feiralivre.core.model.dto.FeiraLivreUpdateDTO;
import com.rbarbioni.feiralivre.core.model.dto.QueryType;
import com.rbarbioni.feiralivre.core.service.FeiraLivreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Api
@RestController
@RequestMapping("/api/v1/feiras")
public class FeiraLivreController {

  private final FeiraLivreService feiraLivreService;

  @Autowired
  public FeiraLivreController(FeiraLivreService feiraLivreService) {
    this.feiraLivreService = feiraLivreService;
  }

  @ApiOperation(value = "Create FeiraLivre")
  @ApiResponses({
          @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "FeiraLivre created"),
          @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "FeiraLivre bad request")
  })
  @PostMapping
  public ResponseEntity create(@Valid @RequestBody FeiraLivre feiraLivre) {
    this.feiraLivreService.save(feiraLivre);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @ApiOperation(value = "Return FeiraLivre paged")
  @ApiResponses({
          @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"),
  })
  @GetMapping
  public Page<FeiraLivre> find(
      @RequestParam(value = "q") QueryType queryType,
      @RequestParam(value = "v") String value,
      Pageable pageable) {
    return this.feiraLivreService.find(queryType, value, pageable);
  }

  @ApiOperation(value = "Return FeiraLivre by registro")
  @ApiResponses({
      @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "FeiraLivre not found"),
      @ApiResponse(code = HttpServletResponse.SC_OK, message = "FeiraLivre found")
  })
  @GetMapping("/{registro}")
  public FeiraLivre find(@PathVariable("registro") String registro) {
    return this.feiraLivreService.findByRegistro(registro);
  }

  @ApiOperation(value = "Update the FeiraLivre by registro")
  @ApiResponses({
      @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "FeiraLivre updated"),
      @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "FeiraLivre not found"),
      @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "FeiraLivre bad request")
  })
  @PutMapping("/{registro}")
  public FeiraLivre update(
      @PathVariable("registro") String registro, @Valid @RequestBody FeiraLivreUpdateDTO feiraLivre) {
    return this.feiraLivreService.update(registro, feiraLivre);
  }

  @ApiOperation(value = "Delete the FeiraLivre by registro")
  @ApiResponses({
    @ApiResponse(code = HttpServletResponse.SC_NO_CONTENT, message = "FeiraLivre deleted"),
    @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "FeiraLivre not found")
  })
  @DeleteMapping("/{registro}")
  public HttpStatus delete(@PathVariable("registro") String registro) {
    this.feiraLivreService.delete(registro);
    return HttpStatus.NO_CONTENT;
  }
}
