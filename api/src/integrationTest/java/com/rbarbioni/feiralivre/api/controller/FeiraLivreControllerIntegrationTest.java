package com.rbarbioni.feiralivre.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;;

import com.rbarbioni.feiralivre.api.ApiApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

//@ExtendWith(SpringExtension.class)
//@WebMvcTest(controllers = FreiraLivreController.class)
//@SpringBootTest(classes = FreiraLivreController .class)
//@WebMvcTest
//@AutoConfigureMockMvc
//@WebMvcTest(FreiraLivreController.class)
@ContextConfiguration(classes= FeiraLivreConfigurationIntegrationTest.class)
//@ComponentScan(basePackages = "com.rbarbioni.feiralivre.*")
//@WebMvcTest(controllers = FreiraLivreController.class)
//@SpringBootTest
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FeiraLivreControllerIntegrationTest {

//    @Autowired
//    private MockMvc mockMvc;
//    @Autowired
    TestRestTemplate rest;

    @Test
    public void shouldReturnStatusOK() throws Exception {
//        mockMvc.perform(get("/api/v1/feiras")).andDo(print()).andExpect(status().isOk());
        System.out.println("rest = " + rest);
    }
}
