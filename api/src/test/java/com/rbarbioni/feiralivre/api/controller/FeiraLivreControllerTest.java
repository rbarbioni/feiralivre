package com.rbarbioni.feiralivre.api.controller;

import com.rbarbioni.feiralivre.core.model.FeiraLivre;
import com.rbarbioni.feiralivre.core.model.dto.FeiraLivreUpdateDTO;
import com.rbarbioni.feiralivre.core.service.FeiraLivreService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class FeiraLivreControllerTest {

    @Mock private FeiraLivreService service;

    @InjectMocks
    private FeiraLivreController controller;

    @Test
    public void shouldSaveNewFeiraLivreAndReturnStatusCreated(){
        final var feiraLivre = new FeiraLivre();
        when(service.save(any(FeiraLivre.class))).thenReturn(feiraLivre);
        final var responseEntity = this.controller.create(feiraLivre);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    public void shouldUpdateFeiraLivreAndReturnStatusNoContent(){
        final var feiraLivre = new FeiraLivreUpdateDTO();
        final var registro = UUID.randomUUID().toString();
        when(service.update(registro, any(FeiraLivreUpdateDTO.class))).thenReturn(new FeiraLivre());
        final var responseEntity = this.controller.update(registro, feiraLivre);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.NO_CONTENT);
    }
}
