package com.rbarbioni.feiralivre.core.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class FeiraLivreCreateDTO extends FeiraLivreBaseDTO {

  private final String registro;

  @JsonCreator
  public FeiraLivreCreateDTO(
      @NotNull @JsonProperty("registro") String registro,
      String nome,
      String regiao5,
      String distrito,
      String bairro) {
    super(nome, regiao5, distrito, bairro);
    this.registro = registro;
  }

  public String getRegistro() {
    return registro;
  }
}
