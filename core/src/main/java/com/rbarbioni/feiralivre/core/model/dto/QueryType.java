package com.rbarbioni.feiralivre.core.model.dto;

public enum QueryType {
  DISTRITO,
  REGIAO_5,
  NOME_FEIRA,
  BAIRRO,
}
