package com.rbarbioni.feiralivre.core.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

abstract class FeiraLivreBaseDTO {

  private final String nome;
  private final String regiao5;
  private final String distrito;
  private final String bairro;

  @JsonCreator
  public FeiraLivreBaseDTO(
      @NotNull @JsonProperty("nome_feira") String nome,
      @NotNull @JsonProperty("regiao_5") String regiao5,
      @NotNull @JsonProperty("distrito") String distrito,
      @NotNull @JsonProperty("bairro") String bairro) {
    this.nome = nome;
    this.regiao5 = regiao5;
    this.distrito = distrito;
    this.bairro = bairro;
  }

  public String getNome() {
    return nome;
  }

  public String getRegiao5() {
    return regiao5;
  }

  public String getDistrito() {
    return distrito;
  }

  public String getBairro() {
    return bairro;
  }
}
