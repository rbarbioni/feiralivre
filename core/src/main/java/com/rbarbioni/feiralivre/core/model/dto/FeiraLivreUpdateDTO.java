package com.rbarbioni.feiralivre.core.model.dto;

public class FeiraLivreUpdateDTO extends FeiraLivreBaseDTO {

  public FeiraLivreUpdateDTO(String nome, String regiao5, String distrito, String bairro) {
    super(nome, regiao5, distrito, bairro);
  }
}
