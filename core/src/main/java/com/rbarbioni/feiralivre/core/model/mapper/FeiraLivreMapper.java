package com.rbarbioni.feiralivre.core.model.mapper;

import com.rbarbioni.feiralivre.core.model.FeiraLivre;
import com.rbarbioni.feiralivre.core.model.dto.FeiraLivreCreateDTO;
import com.rbarbioni.feiralivre.core.model.dto.FeiraLivreUpdateDTO;

public class FeiraLivreMapper {
  public static FeiraLivre merge(FeiraLivre feiraLivre, FeiraLivreUpdateDTO dto) {
    feiraLivre.update(dto);
    return feiraLivre;
  }
}
