package com.rbarbioni.feiralivre.core.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rbarbioni.feiralivre.core.model.dto.FeiraLivreUpdateDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "feiralivre")
public class FeiraLivre {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(name = "registro", updatable = false, unique = true, nullable = false)
  private String registro;

  @NotNull
  @Column(name = "nome_feira")
  private String nome;

  @NotNull
  @Column(name = "set_cens")
  private Long setCens;

  @NotNull
  @Column(name = "logradouro")
  private String logradouro;

  @Column(name = "numero")
  private String numero;

  @NotNull
  @Column(name = "bairro")
  private String bairro;

  @NotNull
  @Column(name = "codDist")
  private Long codDist;

  @NotNull
  @Column(name = "distrito")
  private String distrito;

  @NotNull
  @JsonProperty("lat")
  private Double latitude;

  @NotNull
  @JsonProperty("long")
  private Double longitude;

  @NotNull
  @Column(name = "regiao_5")
  private String regiao5;

  @NotNull
  @Column(name = "regiao_8")
  private String regiao8;

  @NotNull
  @Column(name = "areap")
  private Long areap;

  @NotNull
  @JsonProperty("cod_sub_pref")
  private Long codSubPref;

  @NotNull
  @JsonProperty("sub_prefe")
  private String subPrefe;

  @NotNull
  @JsonProperty("referencia")
  private String referencia;

  public FeiraLivre() {}

  @JsonCreator
  public FeiraLivre(
      @JsonProperty("registro") String registro,
      @JsonProperty("nome_feira") String nome,
      @JsonProperty("set_cens") Long setCens,
      @JsonProperty("logradouro") String logradouro,
      @JsonProperty("numero") String numero,
      @JsonProperty("bairro") String bairro,
      @JsonProperty("cod_dist") Long codDist,
      @JsonProperty("distrito") String distrito,
      @JsonProperty("lat") Double latitude,
      @JsonProperty("long") Double longitude,
      @JsonProperty("regiao_5") String regiao5,
      @JsonProperty("regiao_8") String regiao8,
      @JsonProperty("areap") Long areap,
      @JsonProperty("cod_sub_pref") Long codSubPref,
      @JsonProperty("sub_prefe") String subPrefe,
      @JsonProperty("referencia") String referencia) {
    this();
    this.registro = registro;
    this.nome = nome;
    this.setCens = setCens;
    this.logradouro = logradouro;
    this.numero = numero;
    this.bairro = bairro;
    this.codDist = codDist;
    this.distrito = distrito;
    this.latitude = latitude;
    this.longitude = longitude;
    this.regiao5 = regiao5;
    this.regiao8 = regiao8;
    this.areap = areap;
    this.codSubPref = codSubPref;
    this.subPrefe = subPrefe;
    this.referencia = referencia;
  }

  public void update(FeiraLivreUpdateDTO dto) {
    this.nome = dto.getNome();
    this.regiao5 = dto.getRegiao5();
    this.distrito = dto.getDistrito();
    this.bairro = dto.getBairro();
  }

  @JsonGetter
  public Long getId() {
    return id;
  }

  public String getRegistro() {
    return registro;
  }

  public String getNome() {
    return nome;
  }

  public Long getSetCens() {
    return setCens;
  }

  public String getLogradouro() {
    return logradouro;
  }

  public String getNumero() {
    return numero;
  }

  public String getBairro() {
    return bairro;
  }

  public Long getCodDist() {
    return codDist;
  }

  public String getDistrito() {
    return distrito;
  }

  public Double getLatitude() {
    return latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public String getRegiao5() {
    return regiao5;
  }

  public String getRegiao8() {
    return regiao8;
  }

  public Long getAreap() {
    return areap;
  }

  public Long getCodSubPref() {
    return codSubPref;
  }

  public String getSubPrefe() {
    return subPrefe;
  }

  public String getReferencia() {
    return referencia;
  }
}
