package com.rbarbioni.feiralivre.core.service;

import com.rbarbioni.feiralivre.core.model.FeiraLivre;
import com.rbarbioni.feiralivre.core.model.dto.FeiraLivreUpdateDTO;
import com.rbarbioni.feiralivre.core.model.dto.QueryType;
import com.rbarbioni.feiralivre.core.model.mapper.FeiraLivreMapper;
import com.rbarbioni.feiralivre.core.repository.FeiraLivreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;

@Service
public class FeiraLivreService {

  private final FeiraLivreRepository repository;

  @Autowired
  public FeiraLivreService(FeiraLivreRepository repository) {
    this.repository = repository;
  }

  public Page<FeiraLivre> find(QueryType queryType, String queryValue, Pageable pageable) {
    switch (queryType) {
      case DISTRITO:
        return this.repository.findByDistrito(queryValue, pageable);
      case REGIAO_5:
        return this.repository.findByRegiao5(queryValue, pageable);
      case NOME_FEIRA:
        return this.repository.findByNome(queryValue, pageable);
      case BAIRRO:
        return this.repository.findByBairro(queryValue, pageable);
      default:
        return new PageImpl<>(Collections.emptyList());
    }
  }

  public FeiraLivre findByRegistro(String registro) {
    return this.repository
        .findByRegistro(registro)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  public FeiraLivre save(FeiraLivre feiraLivre) {
    return this.repository.save(feiraLivre);
  }

  public FeiraLivre update(String registro, FeiraLivreUpdateDTO feiraLivreUpdateDTO) {
    return this.repository
        .findByRegistro(registro)
        .map(f -> this.repository.save(FeiraLivreMapper.merge(f, feiraLivreUpdateDTO)))
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  public void delete(String registro) {
    this.repository
        .findByRegistro(registro)
        .ifPresentOrElse(
            f -> repository.deleteById(f.getId()),
            () -> {
              throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            });
  }
}
