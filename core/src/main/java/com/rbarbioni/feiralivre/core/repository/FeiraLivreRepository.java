package com.rbarbioni.feiralivre.core.repository;

import com.rbarbioni.feiralivre.core.model.FeiraLivre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FeiraLivreRepository extends PagingAndSortingRepository<FeiraLivre, Long> {

  Optional<FeiraLivre> findByRegistro(String registro);

  Page<FeiraLivre> findByDistrito(String distrito, Pageable pageable);

  Page<FeiraLivre> findByRegiao5(String regiao5, Pageable pageable);

  Page<FeiraLivre> findByNome(String nome, Pageable pageable);

  Page<FeiraLivre> findByBairro(String bairro, Pageable pageable);

  void deleteByRegistro(String registro);
}
