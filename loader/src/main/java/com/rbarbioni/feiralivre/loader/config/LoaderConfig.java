package com.rbarbioni.feiralivre.loader.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoaderConfig {

    @Bean
    public CsvMapper defineCsvMapper(){
        return new CsvMapper()
                .enable(CsvParser.Feature.WRAP_AS_ARRAY)
                .enable(CsvParser.Feature.EMPTY_STRING_AS_NULL)
                .enable(CsvParser.Feature.INSERT_NULLS_FOR_MISSING_COLUMNS)
                .disable(CsvParser.Feature.FAIL_ON_MISSING_COLUMNS)
                .enable(CsvParser.Feature.TRIM_SPACES);

    }
}
