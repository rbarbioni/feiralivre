package com.rbarbioni.feiralivre.loader.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FeiraLivreCsvRow {

  @JsonProperty("ID")
  private Long id;

  @JsonProperty("NOME_FEIRA")
  private String nomeFeira;

  @JsonProperty("REGISTRO")
  private String registro;

  @JsonProperty("LOGRADOURO")
  private String logradouro;

  @JsonProperty("NUMERO")
  private String numero;

  @JsonProperty("BAIRRO")
  private String bairro;

  @JsonProperty("CODDIST")
  private Long codDist;

  @JsonProperty("DISTRITO")
  private String distrito;

  @JsonProperty("REGIAO5")
  private String regiao5;

  @JsonProperty("REGIAO8")
  private String regiao8;

  @JsonProperty("LONG")
  private Double longitude;

  @JsonProperty("LAT")
  private Double latitude;

  @JsonProperty("AREAP")
  private Long areap;

  @JsonProperty("CODSUBPREF")
  private Long codSubPref;

  @JsonProperty("SUBPREFE")
  private String subPrefe;

  @JsonProperty("REFERENCIA")
  private String referencia;

  @JsonProperty("SETCENS")
  private Long setCens;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Long getSetCens() {
    return setCens;
  }

  public void setSetCens(Long setCens) {
    this.setCens = setCens;
  }

  public Long getAreap() {
    return areap;
  }

  public void setAreap(Long areap) {
    this.areap = areap;
  }

  public Long getCodDist() {
    return codDist;
  }

  public void setCodDist(Long codDist) {
    this.codDist = codDist;
  }

  public String getDistrito() {
    return distrito;
  }

  public String getSubPrefe() {
    return subPrefe;
  }

  public void setSubPrefe(String subPrefe) {
    this.subPrefe = subPrefe;
  }

  public void setDistrito(String distrito) {
    this.distrito = distrito;
  }

  public Long getCodSubPref() {
    return codSubPref;
  }

  public void setCodSubPref(Long codSubPref) {
    this.codSubPref = codSubPref;
  }

  public String getRegiao5() {
    return regiao5;
  }

  public void setRegiao5(String regiao5) {
    this.regiao5 = regiao5;
  }

  public String getRegiao8() {
    return regiao8;
  }

  public void setRegiao8(String regiao8) {
    this.regiao8 = regiao8;
  }

  public String getNomeFeira() {
    return nomeFeira;
  }

  public void setNomeFeira(String nomeFeira) {
    this.nomeFeira = nomeFeira;
  }

  public String getRegistro() {
    return registro;
  }

  public void setRegistro(String registro) {
    this.registro = registro;
  }

  public String getLogradouro() {
    return logradouro;
  }

  public void setLogradouro(String logradouro) {
    this.logradouro = logradouro;
  }

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getBairro() {
    return bairro;
  }

  public void setBairro(String bairro) {
    this.bairro = bairro;
  }

  public String getReferencia() {
    return referencia;
  }

  public void setReferencia(String referencia) {
    this.referencia = referencia;
  }
}
