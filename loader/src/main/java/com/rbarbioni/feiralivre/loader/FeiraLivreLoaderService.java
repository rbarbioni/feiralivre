package com.rbarbioni.feiralivre.loader;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.rbarbioni.feiralivre.core.service.FeiraLivreService;
import com.rbarbioni.feiralivre.loader.model.FeiraLivreCsvRow;
import com.rbarbioni.feiralivre.loader.model.mapper.FeiraLivreMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class FeiraLivreLoaderService implements ApplicationRunner {

  private final static Logger LOG = LoggerFactory.getLogger(FeiraLivreLoaderService.class);
  private final Environment environment;
  private final FeiraLivreService feiraLivreService;
  private final Resource resource;
  private final CsvMapper csvMapper;

  @Autowired
  public FeiraLivreLoaderService(
      Environment environment,
      FeiraLivreService feiraLivreService,
      @Value("classpath:DEINFO_AB_FEIRASLIVRES_2014.csv") Resource resource,
      CsvMapper csvMapper) {
    this.environment = environment;
    this.feiraLivreService = feiraLivreService;
    this.resource = resource;
    this.csvMapper = csvMapper;
  }

  @Override
  public void run(ApplicationArguments args) {
    boolean enabled = Boolean.parseBoolean(this.environment.getProperty("feiralivre.loader.enabled"));
    if(!enabled){
      LOG.info("Loader finish. Argument 'feiralivre.loader.enabled' no exists or false");
      System.exit(0);
    }

    try {
      final MappingIterator<FeiraLivreCsvRow> it =
          csvMapper
              .readerFor(FeiraLivreCsvRow.class)
              .with(CsvSchema.emptySchema()
              .withHeader())
              .readValues(resource.getInputStream());
      while (it.hasNext()) {
        FeiraLivreCsvRow row = it.next();
        LOG.debug("Loader inserting {}, {}", row.getRegistro(), row.getNomeFeira());
        try {
          this.feiraLivreService.save(FeiraLivreMapper.from(row));
        } catch (DataIntegrityViolationException e){
          LOG.warn("Loader {} {} already exists", row.getRegistro(), row.getNomeFeira());
        }
      }
    } catch (Exception e) {
      LOG.error("Loader error {}", e.getMessage(), e);
      System.exit(1);
    } finally {
      System.exit(0);
    }
  }
}
