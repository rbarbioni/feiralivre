package com.rbarbioni.feiralivre.loader.model.mapper;

import com.rbarbioni.feiralivre.core.model.FeiraLivre;
import com.rbarbioni.feiralivre.loader.model.FeiraLivreCsvRow;

public class FeiraLivreMapper {
  public static FeiraLivre from(FeiraLivreCsvRow row) {
    return new FeiraLivre(
        row.getRegistro(),
        row.getNomeFeira(),
        row.getSetCens(),
        row.getLogradouro(),
        row.getNumero(),
        row.getBairro(),
        row.getCodDist(),
        row.getDistrito(),
        row.getLatitude(),
        row.getLongitude(),
        row.getRegiao5(),
        row.getRegiao8(),
        row.getAreap(),
        row.getCodSubPref(),
        row.getSubPrefe(),
        row.getReferencia());
  }
}
